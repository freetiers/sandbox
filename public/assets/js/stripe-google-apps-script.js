/**
 *
 *  Author  FreeTiers
 *  Email   contact@freetiers.com
 *  Web     https://www.freetiers.com
 *
 **/

const STRIPE_API_KEY = "sk_test_51GqRyLF0k36uXbodDqwFx15Sih2FkA6nus0imMjT5TrmusTZz6XptixPbQRVP1A3WYvXcJmsti39gsTDRUajlPep00chx4R2bb";/*"<< Stripe API Key >>";*/
const STRIPE_RETURN_URL = "https://freetiers.com" ;/*"<< Stripe Return URL  >>";*/ // The URL where the customer is redirected after completing a billing portal session.

const GMAIL_ALIAS = "contact@freetiers.com";//"<< Your Preferred Gmail Alias >>";
const EMAIL_DISPLAY_NAME = "FreeTiers"; //"<< The name to be displayed in Your Email >>";

const LOGS_SHEET_NAME = "logs";//"<< The sheet where you want to store your Stripe logs. >>";
const ORDERS_FULFILLMENT_SHEET_NAME = "orders";/* //"<< The sheet where you want to track the fulfillment of your Stripe orders. //>>";*/
const CUSTOMERS_SHEET_NAME = "customers";//"<< The sheet where you want to store your Stripe customers. >>";
const SUBSCRIPTIONS_SHEET_NAME = "subscriptions"; //"<< The sheet where you want to store your Stripe subscriptions. >>";

const CUSTOMERS_SHEET_ID_COLUMN_HEADER = "id"; // The header of the column where the customers' ids are stored.
const SUBSCRIPTIONS_SHEET_ID_COLUMN_HEADER = "id"; // The header of the column where the customers' ids are stored.

const ONE_TIME_PAYMENT_EMAIL_SUBJECT = "<< The subject of the email to be sent to customers after a successful one time payment. >>";
const SUBSCRIPTION_EMAIL_SUBJECT = "<< The subject of the email to be sent to customers after a successful subscription. >>";
const PORTAL_REQUEST_EMAIL_SUBJECT = "<< The subject of the email to be sent to customers after a customer portal access request. >>";
const PRODUCT_ACCESS_EMAIL_SUBJECT = "<< The subject of the email to be sent to customers after a product access request. >>";
const OTHER_REQUEST_EMAIL_SUBJECT = "<< The subject of the email to be sent to customers after an uncategorized request. >>";

const CUSTOMER_PORTAL_REQUEST_PAGE = "<< The page where your subscribers can request new billing portal sessions. >>";
const CUSTOMER_CONTACT_PAGE = "<< The page where your customers can contact you for uncategorized requests. >>";

const CUSTOMERS_PARENT_FOLDER_NAME = ""; // << The folder where all the customers' folders will be stored. >>



/**
 * Handle the webhook events from Stripe
 *
 * @param {object} description The post data
 * @return 
 */

function doPost(e){

  
  var jsonString = e.postData.getDataAsString();
  var event = JSON.parse(jsonString);
  var getHookType = event.type;
  
  if(getHookType){
	switch(getHookType){
	  
		  case "checkout.session.completed":
			var hookType = "New Checkout Session";

			Logger.log("hookType 44 :\t" + hookType);
			
			var sessionId = event.data.object.id;
			var expandedSessionObject = getSessionbyId(sessionId);

			fulfillOrder(expandedSessionObject);
			
									
			break;

		  
		  
		  case "customer.created":
			var hookType = "New Customer"
			var customerId = event.data.object.id;
			var customerEmail = event.data.object.email;
			
			break;

		  case "customer.updated":       
			var hookType = "Updated Customer";
			var updatedCustomer = event.data.object;
			var customerPreviousAttributes = event.data.previous_attributes; // The names and the previous values of the updated attributes of the subscription object.
			var customerUpdate = updateCustomer(updatedCustomer, customerPreviousAttributes);
			
			break;
		
		
			
		  case "customer.subscription.created":       
			var hookType = "New Paid Subscriber";
			var customerId = event.data.object.customer;
			
			break;
			
		  case "customer.subscription.updated":       
			var hookType = "Updated Subscription";
			var updatedSubscription = event.data.object;
			var subscriptionPreviousAttributes = event.data.previous_attributes; // The names and the previous values of the updated attributes of the subscription object.
			var subscriptionUpdate = updateSubscription(updatedSubscription, subscriptionPreviousAttributes);
			
			break;


		  
		  case "charge.succeeded":
			var hookType = "Successful Payment";
			var customerId = event.data.object.customer;
			
			break;	  
		  
		  case "charge.failed":
			var hookType = "Failed Payment";
			var customerId = event.data.object.customer;
			
			break;	  
			
			

		  case "invoice.paid":
			var hookType = "Invoice Paid";
			
			break;	  

		  case "invoice.payment_succeeded":
			var hookType = "Successful Invoice Payment Attempt";
			
			break;	  
			
		  case "invoice.payment_failed":
			var hookType = "Failed Invoice Payment Attempt";
			
			break;	  
		
	}
  
    //Insert the data into the logs sheet  

	recordLog(hookType, event.data, customerEmail, customerId);
  
  }	else if(e.parameter.request) {
	  
	  var customerRequest = e.parameter.request;
	  var customerEmail = e.parameter.email;
	  var customerId = getCustomerIdbyEmail(customerEmail); //  get customer id by email based on the customers sheet data.
	  
	  switch(request){
		  
		  case "portal":
			var portalLink = createCustomerPortalSession(customerId);
			var emailBody = makeCustomerPortalSessionEmail(portalLink);
			var emailSubject = PORTAL_REQUEST_EMAIL_SUBJECT;
			var sendTo = customerEmail;
			var sent = sendEmailToCustomer(emailSubject, emailBody, sendTo);
			
			break;

		  case "product":
			var customerFolderLink = getCustomerFolderLink(customerId);
			var emailBody = makeCustomerProductAccessEmail(customerFolderLink);
			var emailSubject = PRODUCT_ACCESS_EMAIL_SUBJECT;
			var sent = sendEmailToCustomer(emailSubject, emailBody, sendTo);
			
			break;
			
		  case "other":
		  
			var customerContactPageLink = CUSTOMER_CONTACT_PAGE;
			var emailBody = makeOtherRequestEmail(customerContactPageLink);
			var emailSubject = OTHER_REQUEST_EMAIL_SUBJECT;
			var sent = sendEmailToCustomer(emailSubject, emailBody, sendTo);

		  
			break;
			
	  }
  }

  return HtmlService.createHtmlOutput(200);
};


/**
 * Get the data in a sheet by removing the header and building JS objects for each row.
 *
 * @param {string} description The sheet's name.
 * @return {string} The sheet's data as JS objects.
 */

function getSheetDataAsObjects(sheetName){
	
	var sheet = switchToSheet(sheetName);
	var dataRange = sheet.getDataRange();
	var data = dataRange.getValues();
	var header = data.shift();
	
	var dataObjects = data.map(function(values){
		return header.reduce(function(dataObject, property, index){
			dataObject[property] = values[index];
			return dataObject;
		}, {})
	});
	
	
	return dataObjects;;
}



/**
 * Retrieve an existing customer'id by its email.
 * from the data in the customers' sheet
 *
 * @param {string} description The customer's email.
 * @return {string} The customer's id
 */

function getCustomerIdbyEmail(customerEmail){
	
	var customerId;
	
	var dataObjects = getSheetDataAsObjects(CUSTOMERS_SHEET_NAME)
	
	dataObjects.forEach(function(row, rowIndex){
		if(row.email === customerEmail){
			customerId = row.id;
		}
	});
	
	return customerId;
}

/**
 * Retrieve the id of the customers' parent folder in your Google Drive.
 *
 * @return {string} description The id of the customers parent folder.
 */

function getCustomersParentFolderId(){
	
	var customersParentFolderName = CUSTOMERS_PARENT_FOLDER_NAME;
	
	var customersParentFolderId ;
	
	const foldersByCustomersParentFolderName = DriveApp.getFoldersByName(customersParentFolderName);
	
	while (foldersByCustomersParentFolderName.hasNext()){
		folder = foldersByCustomersParentFolderName.next();
		if(folder.getName() === customersParentFolderName){
			customersParentFolderId = folder.getId();
			return customersParentFolderId;
		}
	}
			
	return false ;
}

/**
 * Retrieve the id of a customer's dedicated folder in your Google Drive
 *
 * @param {string} description The customer's email.
 * @return {string} description The id of the customer's folder.
 */

function getCustomerFolderId(customerEmail){
	
	var customersParentFolderId = getCustomersParentFolderId();
	
	var customerFolderName = customerEmail;
	
	const customersParentFolder = DriveApp.getFolderById(customersParentFolderId);
	
	const customerFolders = customersParentFolder.getFoldersByName(customerFolderName);

	while (customerFolders.hasNext()){
		folder = customerFolders.next();
		if(folder.getName() === customerFolderName){
			customerFolderId = folder.getId();
			return customerFolderId;
		}
	}
			
	return false ;
}


/**
 * Create a shortcut for a file stored in your Google Drive .
 *
 * @return {string} The id of the shortcut's target.
 * @param {string} description The name of the shortcut.
 * @return {string} The id of the shortcut's parent folder.
 */
function createShortcut(targetId, shortcutName, parentFolderId) {
	
	const resource = {
		shortcutDetails: { targetId: targetId },
		title: shortcutName,
		mimeType: "application/vnd.google-apps.shortcut",
	};
	
	if (parentFolderId){
		resource.parents = [{ id: parentFolderId }];
	}
	
	const shortcut = Drive.Files.insert(resource);
	
	return shortcut.id;
}

/**
 * Share a digital product in your Google Drive ( ... or not later on) with a customer.
 *
 * @param {string} description The customer email.
 * @param {string} description The file or folder id of the digital product.
 * @return {string} The link of the shared file.
 */

function shareProductWithCustomer(customerEmail, fileOrFolderId){
	
	Drive.Permissions.insert(
		{
		  role: "reader", // or "writer" or "commenter"
		  type: "user",
		  value: customerEmail
		},
		fileOrFolderId,
		{
		  supportsAllDrives: true,
		  sendNotificationEmails: false
		}
	);
	
}


/**
 * Retrieve a Checkout session by its id with the customer, subscription, 
 * payment_intent and line_items properties expanded.
 *
 * @param {string} description The Stripe Checkout session id.
 * @return Stripe checkout session object
 */

function getSessionbyId(sessionId){
  

  /*
  For debugging :
  sessionId = "cs_test_a1liyDNEffs0vVxsUQ06btdoUM3emkBkGCHfe52vphTBukj2hS7IXFEDSJ";
  */

  const response = UrlFetchApp.fetch(
    `https://api.stripe.com/v1/checkout/sessions/${sessionId}?expand[]=customer&expand[]=subscription&expand[]=payment_intent&expand[]=line_items`,
    {
      method: "GET",
      headers: {
        Authorization: `Bearer ${STRIPE_API_KEY}`,
        "Content-Type": "application/x-www-form-urlencoded",
      },
      muteHttpExceptions: true,
    }
  );

  Logger.log("response 156 :\t" + response);
  
  const data = JSON.parse(response); 

  Logger.log("data 160  :\t" + JSON.stringify(data));

  if (!data) {
    // Something went wrong
    return {};
  }

  return data;
};


/**
 * Fulfill the order for a given Checkout session.
 *
 * @param {object} description The session object with the customer, subscription, payment_intent, and line_items attributes expanded.
 * @return 
 */

function fulfillOrder(expandedSessionObject){

  /*
  
  For debugging
  
  var sessionId = "cs_test_a1liyDNEffs0vVxsUQ06btdoUM3emkBkGCHfe52vphTBukj2hS7IXFEDSJ";

  expandedSessionObject = getSessionbyId(sessionId);
  */


	var sheet = switchToSheet(ORDERS_FULFILLMENT_SHEET_NAME);
	var lastRow = sheet.getLastRow();  

	var sessionCustomer = expandedSessionObject.customer;
	Logger.log("sessionCustomer 191 :\t" +  JSON.stringify(sessionCustomer));
	var customerRecord = recordCustomer(sessionCustomer);

	var sessionPaymentIntent = expandedSessionObject.payment_intent;
	Logger.log("sessionPaymentIntent 195 :\t" + JSON.stringify(sessionPaymentIntent));
	// The payment_intent object has a charges attribute with a data array containing the last associated charge only.
	var sessionCharge = sessionPaymentIntent.charges.data[0];
	Logger.log("sessionCharge 197 :\t" + JSON.stringify(sessionCharge));
	Logger.log("sessionCharge billing details 198 :\t" + JSON.stringify(sessionCharge.billing_details.email));

	// The line_items attribute of the checkout session contains only one product as buying multiple products at once isn't implemented yet.
	var sessionLineItems = expandedSessionObject.line_items.data[0];
	Logger.log("sessionLineItems 201 :\t" + JSON.stringify(sessionLineItems));

	var sendTo = sessionCharge.billing_details.email;

	var emailSubject="";
	var emailBody="";
	
	var sessionMode = expandedSessionObject.mode;
	Logger.log("sessionMode 209 :\t" + sessionMode);

	if(sessionMode == "subscription"){

		var sessionSubscription = expandedSessionObject.subscription;
    Logger.log("sessionSubscription 207 :\t" + sessionSubscription);
		var subscriptionRecord = sessionSubscription ? recordSubscription(sessionSubscription) : false;
		
		if(subscriptionRecord && sessionCharge && sessionLineItems){
			emailSubject = SUBSCRIPTION_EMAIL_SUBJECT;
			emailBody = makeSubscriptionEmail(sessionCharge, sessionLineItems);
		}
		
	} else if (sessionMode == "payment"){
		
		if(sessionCharge && sessionLineItems){
			emailSubject = ONE_TIME_PAYMENT_EMAIL_SUBJECT;
			emailBody = makeOneTimePaymentEmail(sessionCharge, sessionLineItems);
		}
		
	} else {
		return false;
	}
  
  
  return ;
};


/**
 * Switch to a sheet with the given name.
 *
 * @param {string} description The name of the sheet.
 * @return The selected sheet.
 */

function switchToSheet(sheetName){

  var ss = SpreadsheetApp.getActiveSpreadsheet();
  var sheet = ss.getSheetByName(sheetName);

  return sheet;
};


/**
 * Record a log entry for a given Stripe webhook event.
 *
 * @param {string} description The type of webhook event.
 * @param {object} description The webhook event.
 * @param {string} description The customer email.
 * @param {string} description The customer id.
 * @return 
 */

function recordLog(hookType, eventData, customerEmail, customerId){

  var timeStamp = new Date();
  var time = Utilities.formatDate(timeStamp, "GMT", "MM/dd/yy, h:mm a");

  var sheet = switchToSheet(LOGS_SHEET_NAME);
  var lastRow = sheet.getLastRow();  
  
  sheet.getRange(lastRow + 1, 1).setValue(time); 
  sheet.getRange(lastRow + 1, 2).setValue(hookType); 
  sheet.getRange(lastRow + 1, 3).setValue(eventData);   
  sheet.getRange(lastRow + 1, 4).setValue(customerEmail);
  sheet.getRange(lastRow + 1, 5).setValue(customerId);



  return ;
};



/**
 * Record the customer from a completed Checkout session in the customers sheet.
 *
 * @param {object} description The customer object.
 * @return 
 */

function recordCustomer(customer){
	
  var lock = LockService.getDocumentLock();
  lock.waitLock(30000); // hold off up to 30 sec to avoid concurrent writing

  var sheet = switchToSheet(CUSTOMERS_SHEET_NAME);
  var lastRow = sheet.getLastRow();  
  
  sheet.getRange(lastRow + 1, 1).setValue(customer.id); 
  sheet.getRange(lastRow + 1, 2).setValue(customer.email); 

  lock.releaseLock();
  
  return ;
};


/**
 * Record the subscription from a completed Checkout session in the subscriptions sheet.
 *
 * @param {object} description The subscription object.
 * @return 
 */

function recordSubscription(subscription){

  var lock = LockService.getDocumentLock();
  lock.waitLock(30000); // hold off up to 30 sec to avoid concurrent writing
  
  var sheet = switchToSheet(SUBSCRIPTIONS_SHEET_NAME);
  var lastRow = sheet.getLastRow();  

  sheet.getRange(lastRow + 1, 1).setValue(subscription.id); 
  sheet.getRange(lastRow + 1, 2).setValue(subscription.current_period_end); 
  sheet.getRange(lastRow + 1, 3).setValue(subscription.customer);   
  sheet.getRange(lastRow + 1, 4).setValue(subscription.items.data.id);
  sheet.getRange(lastRow + 1, 5).setValue(subscription.status);
  
  lock.releaseLock();
  return ;
};

/**
 * Update the records stored in a spreadsheet after a specific *.updated event.
 *
 * @param {string} description The name of the sheet where the records are stored.
 * @param {string} description The header of the column where the ids are stored.
 * @param {object} description The names and the previous values of the attributes that changed.
 * @param {object} description The updated object resulting from the *.updated event.
 * @param {object} description The subscription object.
 * @return 
 */

function updateRecordsOnEvent(sheetName,idColumnHeader, updatedObject, previousAttributes){
	
	var previousAttributesEntries = Object.entries(previousAttributes);
	
	var idColumn = header.indexOf(idColumnHeader);
	
	var dataObjects = getSheetDataAsObjects(sheetName);
	
	dataObjects.forEach(function(row, rowIndex){
		if(row.id == updatedObject.id){
			
			previousAttributesEntries.forEach(entry, index){
				if(data[rowIndex][entry[0]] === entry[1]){
					try {
						data[rowIndex][entry[0]] = updatedObject[entry[0]];
					} catch(e) {
						data[rowIndex][entry[0]] = entry[1];
						Logger.log("Exception at line 553 + "e.message);
					}
					
				}
			}
		}
	});
	
	dataRange.offset(1,0,data.length).setValues(data);
}


/**
 * Update a subscription previously recorded in the subscriptions sheet.
 *
 * @param {object} description The updated subscription object.
 * @param {object} description The previous values of the updated attributes in the subscription object.
 * @return 
 */

function updateSubscription(updatedSubscription, subscriptionPreviousAttributes){
	
	var lock = LockService.getDocumentLock();
	lock.waitLock(30000); // hold off up to 30 sec to avoid concurrent writing
	
	var sheet = SUBSCRIPTIONS_SHEET_NAME;
	var idColumnHeader = SUBSCRIPTIONS_SHEET_ID_COLUMN_HEADER || "id";
	
	updateRecordsOnEvent(sheet, idColumnHeader, updatedSubscription, subscriptionPreviousAttributes);
	
	lock.releaseLock();
  
  return ;
};


/**
 * Update a customer previously recorded in the customers sheet.
 *
 * @param {object} description The updated customer object.
  * @param {object} description The previous values of the updated attributes in the customer object.
 * @return 
 */

function updateCustomer(updatedCustomer, customerPreviousAttributes){

	var lock = LockService.getDocumentLock();
	lock.waitLock(30000); // hold off up to 30 sec to avoid concurrent writing
	
	var sheet = CUSTOMERS_SHEET_NAME;
	var idColumnHeader = CUSTOMERS_SHEET_ID_COLUMN_HEADER || "id";
	
	updateRecordsOnEvent(sheet, idColumnHeader, updatedSubscription, subscriptionPreviousAttributes);
	
	lock.releaseLock();
    
  return ;
};


/**
 * Send an email notification to the customer
 *
 * @param {string} description The email subject
 * @param {string} description The email body
 * @param {string} description The customer's email address
 * @return 
 */

function sendEmailToCustomer(emailSubject, emailBody, sendTo) {  

  var sendFrom = GMAIL_ALIAS;
  var replyTo = GMAIL_ALIAS;
  var name = EMAIL_DISPLAY_NAME;

  GmailApp.sendEmail(sendTo, emailSubject, emailBody, {
    from: sendFrom,
    replyTo: replyTo,
    name: name
  });

  return true;
};

/**
 * Make the body of an email to be sent to a subscriber on a new successful payment.
 *
 * @param {object} description The charge object
 * @param {object} description The line_items object
 * @return 
 */

function makeSubscriptionEmail(charge, line_items) {  
  
  var customerName = charge.billing_details.name;
  var productDescription = charge.description;
  var receiptUrl = charge.receipt_url
  var productLink = line_items.price.metadata.product_link;

  var greetings= customerName ? `Hi ${customerName} ! \n\n` : `Hi !`;
  
  var confirmation=`Thanks for your subscription. \n\n `;
  
  var summary=`Here's a summary of what you bought : \n \n
			   Product Description : ${productDescription} \n \n
			   You can get the full receipt here : ${receiptLink}. \n \n`;
			   
  var delivery= productLink ? `Get started with your subscription here : ${productLink} \n \n` : ``;
  
  var farewell=`See You !`;
  
  var support=`Get in touch at ${GMAIL_ALIAS} if you need some help.`;
  
  var emailBody = greetings + confirmation + summary + delivery + farewell + support;

  Logger.log(emailBody);
  return emailBody;
  
};


/**
 * Make the body of an email to be sent to a new customer on a new successful payment.
 *
 * @param {object} description The charge object
 * @param {object} description The line_items object
 * @return 
 */

function makeOneTimePaymentEmail(charge, line_items) {  
  
  var customerName = charge.billing_details.name;
  var productDescription = charge.description;
  var receiptUrl = charge.receipt_url
  var productLink = line_items.price.metadata.product_link;

  var greetings= customerName ? `Hi ${customerName} ! \n\n` : `Hi !`;
  
  var confirmation=`Thanks for your purchase. \n\n `;
  
  var summary=`Here's a summary of what you bought : \n \n
			   Product Description : ${productDescription} \n \n
			   You can get the full receipt here : ${receiptLink}. \n \n`;
			   
  var delivery= productLink ? `As for your download, here it is : ${productLink} \n \n` : ``;
  
  var farewell=`See You !`;
  
  var support=`Get in touch at ${GMAIL_ALIAS} if you need some help.`;
  
  var emailBody = greetings + confirmation + summary + delivery + farewell + support;

  Logger.log(emailBody);
  return emailBody;
  
};


/**
 * Make the body of an email to be sent to a customer with the link to its dedicated product folder.
 *
 * @param {object} description The customer object
 * @return 
 */

function makeCustomerProductAccessEmail(customerFolderLink) {  
  
  var greetings= `Hi ! \n\n`;
  
  var confirmation=`You requested an access to your products. \n\n `;
  
  var summary=`The link below will lead you to your dedicated folder. \n \n
			   All the products you bought from us are stored there. \n \n`;
			   
  var delivery= customerFolderLink ? `Here's is the link: ${customerFolderLink} \n \n` : ``;
  
  var farewell=`See You !`;
  
  var support=`Get in touch at ${GMAIL_ALIAS} if you need some help.`;
  
  var emailBody = greetings + confirmation + summary + delivery + farewell + support;

  Logger.log(emailBody);
  return emailBody;
  
};

/**
 * Make the body of an email to be sent to a customer with the link to its dedicated product folder.
 *
 * @param {object} description The customer object
 * @return 
 */

function makeOtherRequestEmail(customerContactPageLink) {  
  
  var greetings= `Hi ! \n\n`;
  
  var confirmation=`You seem to have a particular request. \n\n `;
  
  var summary=`Follow the link below to share it with us. \n \n`;
			   
  var delivery= customerContactPageLink ? `Here's is the link: ${customerContactPageLink} \n \n` : ``;
  
  var farewell=`See You !`;
  
  var support=`Get in touch at ${GMAIL_ALIAS} if you need some help.`;
  
  var emailBody = greetings + confirmation + summary + delivery + farewell + support;

  Logger.log(emailBody);
  return emailBody;
  
};

/**
 * Make the body of an email to be sent to a customer with the link to a new subscription portal session.
 *
 * @param {object} description The customer object
 * @return 
 */

function makeCustomerPortalSessionEmail(portalLink) {  
  
  var greetings= `Hi ! \n\n`;
  
  var confirmation=`You requested an access to a customer portal session. \n\n `;
  
  var summary=`The link below will initiate a new session : \n \n
			   It will expire if you don't use it right away \n \n
			   If you need a new link, just go to this page for requesting a new session : ${CUSTOMER_PORTAL_REQUEST_PAGE} \n \n`;
			   
  var delivery= portalLink ? `As for your customer portal, here is the link: ${portalLink} \n \n` : ``;
  
  var farewell=`See You !`;
  
  var support=`Get in touch at ${GMAIL_ALIAS} if you need some help.`;
  
  var emailBody = greetings + confirmation + summary + delivery + farewell + support;

  Logger.log(emailBody);
  return emailBody;
  
};


/**
 * Create a new portal session for a new customer.
 *
 * @param {string} description The customer id
 * @return 
 */

function createCustomerPortalSession(customerId, returnUrl) {  
  
  const params = {
    customer: customerId,
    return_url: returnUrl || STRIPE_RETURN_URL
  };

  const payload = Object.entries(params)
    .map(([key, value]) =>
      [encodeURIComponent(key), encodeURIComponent(value)].join("=")
    )
    .join("&");

  const response = UrlFetchApp.fetch(
    "https://api.stripe.com/v1/billing_portal/sessions",
    {
      method: "POST",
      headers: {
        Authorization: `Bearer ${STRIPE_API_KEY}`,
        "Content-Type": "application/x-www-form-urlencoded",
      },
      payload,
      muteHttpExceptions: true
    }
  );

  const { url, error } = JSON.parse(response);
  
  return error ? error.message : url;
};